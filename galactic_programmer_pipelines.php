<?php

if (!defined("_ECRIRE_INC_VERSION")) return;


/**
 * Insertion dans le pipeline formulaire_charger (SPIP)
 *
 * Préchargement des valeurs type et severite des tickets dans le cas d'un nouveau ticket
 *
 * @param array $flux
 * 		Le contexte du pipeline
 * @return array $flux
 * 		Le contexte du pipeline modifié
 */
function documentation_formulaire_charger($flux){
	if ($flux['args']['form'] == 'editer_ticket'
		and (!$flux['args']['args'][0] OR $flux['args']['args'][0] == 'oui')
		and !$flux['data']['type']) {
		$flux['data']['type'] = 2;
		$flux['data']['severite'] = 4;
	}
	return $flux;
}


// ajouter automatiquement des title "art30" sur les raccourcis [->art30]
// donc, transformer [->art30] en [|art30->art30]
// ce qui permet a l'integrale de gerer des numeros de pages sur les liens
function documentation_pre_liens($texte){
	// uniquement dans le public
	if (test_espace_prive()) return $texte;

	$regs = $match = array();
	// pour chaque lien
	if (preg_match_all(_RACCOURCI_LIEN, $texte, $regs, PREG_SET_ORDER)) {
		foreach ($regs as $reg) {
			// si le lien est de type raccourcis "art40"
			if (preg_match(_RACCOURCI_URL, $reg[4], $match)) {
				$title = '|' . $match[1] . $match[2];
				// s'il n'y a pas deja ce title
				if (false === strpos($reg[0], $title)) {
					$lien = substr_replace($reg[0], $title, strpos($reg[0], '->'), 0);
					$texte = str_replace($reg[0], $lien, $texte);
				}
			}
		}
	}
	return $texte;
}

// transformer les <a title="art30" href="xxx">zzz</a>
// en <a href="#art30">zzz</a> (enfin <a title="art30" href="#art30">zzz</a> en attendant mieux)
// seulement dans le squelette integrale
// pour que le pdf puisse calculer les numero de page des liens
function documentation_affichage_final($page){
	// uniquement dans le public
	if (test_espace_prive()) return $page;

	if (_request('page') == 'integrale') {
		include_spip('inc/filtres');
		include_spip('inc/lien');
		$as = extraire_balises($page, 'a');
		foreach($as as $a) {
			$classe = extraire_attribut($a, 'class');
			$title = extraire_attribut($a, 'title');

			if ($title) {
				if (in_array($classe, array('spip_glossaire', 'spip_note'))) {
					// les raccourcis [?ecrire/paquet.xml#core] ont un title qu'il faut enlever
					// sinon il est pris comme ancre par DocRaptor (PrinceXML) et affiche un n° de page erroné car l’identifiant n'existe pas.
					// De même : 'Note 1' en title est pris pour un raccourcis d’URL, on le vire
					if (
						($classe == 'spip_glossaire')
						or ($classe == 'spip_note' and extraire_attribut($a, 'rev') == 'footnote')
					) {
						$old_a = $a;
						$a = vider_attribut($a, 'title');
						$page = str_replace($old_a, $a, $page);
					}
					continue;
				}

				elseif (preg_match(_RACCOURCI_URL, $title, $match)) {
					$old_a = $a;
					// on laisse le title parce que a[href|="#"] ne semble pas pris en compte avec DocRaptor (PrinceXML) ...
					# $a = vider_attribut($a, 'title');
					// si l’ancre n'est pas raccourcie (article12 et non art12), on la raccourcit
					if (in_array($match[1], array('article', 'rubrique')) and $match[2]) {
						$title = substr($match[1], 0, 3) . $match[2];
					}
					// si ancre [->12] c'est art12
					if (!$match[1] and $match[2]) {
						$title = 'art' . $match[2];
					}
					$a = inserer_attribut($a, 'href', '#' . $title);
					$a = inserer_attribut($a, 'title', $title);
					$page = str_replace($old_a, $a, $page);
				}
			}
		}
		unset($as, $a, $old_a, $title);
	}

	return $page;
}


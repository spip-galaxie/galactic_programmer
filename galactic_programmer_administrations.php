<?php
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/cextras');
include_spip('base/galactic_programmer');
	
function galactic_programmer_upgrade($nom_meta_base_version,$version_cible){
	$maj 	= array();
	$champs = galactic_programmer_declarer_champs_extras();
	include_spip('base/upgrade');
	cextras_api_upgrade($champs,$maj['create']);
	maj_plugin($nom_meta_base_version, $version_cible,$maj);
}

function galactic_programmer_vider_tables($nom_meta_base_version) {
	$champs = galactic_programmer_declarer_champs_extras();
	cextras_api_vider_tables($champs);
	effacer_meta($nom_meta_base_version);
}


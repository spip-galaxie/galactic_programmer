<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_programmer?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Author :',

	// C
	'champ_auteur' => 'Author',
	'champ_date' => 'Date',
	'champ_id' => 'Id',
	'champ_langue' => 'Lang',
	'champ_resume' => 'Summary',
	'champ_texte' => 'Text',
	'champ_titre' => 'Title',
	'chapitre' => 'Chapter: ',
	'choisir' => 'Choose...',
	'commentaire' => '@nb@ comment',
	'commentaire_aucun' => 'No comments',
	'commentaires' => '@nb@ comments',
	'commentez' => 'Comment on the documentation',
	'creer_nouvelle_suggestion' => 'Make a new suggestion',

	// D
	'description' => 'Description',
	'documentation_papier' => 'Printed documentation !',
	'documentation_papier_complement' => 'To read at leisure...',

	// E
	'editer_suggestion' => 'Edit this suggestion',
	'en_savoir_plus' => 'More',
	'erreur_de_chargement_ajax' => 'AJAX loading error!',
	'erreur_inscription_desactivee' => 'Registration are disabled on this site.',
	'erreur_inscription_session' => 'You are already identified.',
	'exemple' => 'Example',
	'exercice' => 'Exercise',

	// I
	'icones_par' => 'Icons adapted from the theme ',
	'integrale' => 'Complete!',

	// L
	'label_charger_url' => 'Quick access:',
	'label_exemple' => 'Example',
	'label_exercice' => 'Exercise',
	'label_reponse' => 'Answer',
	'lien_sedna' => 'Websites we follow',
	'lien_sedna_img' => 'Sedna',

	// M
	'maj' => 'Updated on ',
	'mentions_legales' => 'Legal notices',

	// N
	'nom' => 'Name',
	'nouvelle_suggestion' => 'New suggestion',

	// P
	'precedent' => 'Previous',
	'proposer_suggestion' => 'Suggest an improvement!',
	'proposer_suggestion_img' => 'Tickets manager',

	// R
	'reponse' => 'Answer',

	// S
	'signaler_coquille' => 'Report an error…',
	'sinscrire' => 'Register',
	'sommaire' => 'Contents',
	'sommaire_livre' => 'Contents',
	'sous_licence' => 'under the',
	'suggestion' => 'Suggestion',
	'suggestions' => 'Suggestions',
	'suivant' => 'Next',
	'suivi' => 'Monitor',
	'suivi_dernieres_modifications_articles' => 'Recent modifications to the articles',
	'suivi_derniers_articles' => 'Recent articles',
	'suivi_derniers_articles_proposes' => 'Last articles proposed',
	'suivi_derniers_commentaires' => 'Recent comments',
	'suivi_description' => 'Monitor the site...',
	'symboles' => 'Symbols',

	// T
	'table_des_matieres' => 'Table of contents',
	'tickets_sur_inscription' => '
		Writing tickets and comments is only available
		for registered users.
	',
	'titre_articles_lies' => 'Additional articles',
	'titre_identification' => 'Identification',
	'titre_inscription' => 'Registration',
	'tout_voir' => 'View all'
);

<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_programmer?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Autor:',

	// C
	'champ_auteur' => 'Autor(es)',
	'champ_date' => 'Fecha',
	'champ_id' => 'Id',
	'champ_langue' => 'Idioma',
	'champ_resume' => 'Resumen',
	'champ_texte' => 'Texto',
	'champ_titre' => 'Título',
	'chapitre' => 'Capítulo: ',
	'choisir' => 'Elegir...',
	'commentaire' => '@nb@ comentario',
	'commentaire_aucun' => 'Ningún comentario',
	'commentaires' => '@nb@ comentarios',
	'commentez' => 'Comentar la documentación',
	'creer_nouvelle_suggestion' => 'Proponer una nueva sugerencia',

	// D
	'description' => 'Descripción',
	'documentation_papier' => '¡Documentación papel!',
	'documentation_papier_complement' => 'Para leer tranquilamente...',

	// E
	'editer_suggestion' => 'Editar esta sugerencia',
	'en_savoir_plus' => '¡Saber más!',
	'erreur_de_chargement_ajax' => '¡Error al cargar AJAX!',
	'erreur_inscription_desactivee' => 'Los registros están desactivados para este sitio.',
	'erreur_inscription_session' => 'Ya está identificado.',
	'exemple' => 'Ejemplo',
	'exercice' => 'Ejercicio',

	// I
	'icones_par' => 'Iconos adaptados del tema',
	'integrale' => '¡Íntegro!',

	// L
	'label_charger_url' => 'Acceso rápido:',
	'label_exemple' => 'Ejemplo',
	'label_exercice' => 'Ejercicio',
	'label_reponse' => 'Respuesta',
	'lien_sedna' => 'Sitios que seguimos',
	'lien_sedna_img' => 'Sedna',

	// M
	'maj' => 'Revisión del ',
	'mentions_legales' => 'Menciones legales',

	// N
	'nom' => 'Nombre',
	'nouvelle_suggestion' => 'Nueva sugerencia',

	// P
	'precedent' => 'Precedente',
	'proposer_suggestion' => '¡Proponga una mejora!',
	'proposer_suggestion_img' => 'Administrador de tickets',

	// R
	'reponse' => 'Respuesta',

	// S
	'signaler_coquille' => 'Indique una errata...',
	'sinscrire' => 'Registrarse',
	'sommaire' => 'Contenido',
	'sommaire_livre' => 'Sumario',
	'sous_licence' => 'bajo licencia',
	'suggestion' => 'Sugerencia',
	'suggestions' => 'Sugerencias',
	'suivant' => 'Siguiente',
	'suivi' => 'Seguimiento',
	'suivi_dernieres_modifications_articles' => 'Últimos cambios en los artículos',
	'suivi_derniers_articles' => 'Últimos artículos',
	'suivi_derniers_articles_proposes' => 'Últimos artículos propuestos',
	'suivi_derniers_commentaires' => 'Últimos comentarios',
	'suivi_description' => 'Seguimiento del sitio...',
	'symboles' => 'Símbolos',

	// T
	'table_des_matieres' => 'Contenido',
	'tickets_sur_inscription' => 'La redacción de tickets o comentarios no es posible más que para las personas identificadas.',
	'titre_articles_lies' => 'Artículos complementarios',
	'titre_identification' => 'Identificación',
	'titre_inscription' => 'Registro',
	'tout_voir' => 'Ver todo'
);

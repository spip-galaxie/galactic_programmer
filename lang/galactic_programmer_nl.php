<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/galactic_programmer?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'auteur' => 'Auteur:',

	// C
	'champ_auteur' => 'Auteur',
	'champ_date' => 'Datum',
	'champ_id' => 'Id',
	'champ_langue' => 'Taal',
	'champ_resume' => 'Samenvatting',
	'champ_texte' => 'Tekst',
	'champ_titre' => 'Titel',
	'chapitre' => 'Hoofdstuk: ',
	'choisir' => 'Kies...',
	'commentaire' => '@nb@ opmerking',
	'commentaire_aucun' => 'Geen opmerkingen',
	'commentaires' => '@nb@ opmerkingen',
	'commentez' => 'Opmerking over de documentatie',
	'creer_nouvelle_suggestion' => 'Doe een suggestie',

	// D
	'description' => 'Omschrijving',
	'documentation_papier' => 'Gedrukte documentatie!',
	'documentation_papier_complement' => 'Om op je gemak te lezen...',

	// E
	'editer_suggestion' => 'Pas deze suggestie aan',
	'en_savoir_plus' => 'Meer',
	'erreur_de_chargement_ajax' => 'AJAX laadfout!',
	'erreur_inscription_desactivee' => 'Registratie is op deze site niet mogelijk.',
	'erreur_inscription_session' => 'Je bent al aangemeld.',
	'exemple' => 'Voorbeeld',
	'exercice' => 'Oefening',

	// I
	'icones_par' => 'Ikonen aangepast met thema ',
	'integrale' => 'Volledig!',

	// L
	'label_charger_url' => 'Snelle toegang:',
	'label_exemple' => 'Voorbeeld',
	'label_exercice' => 'Oefening',
	'label_reponse' => 'Antwoord',
	'lien_sedna' => 'Websites die we volgen',
	'lien_sedna_img' => 'Sedna',

	// M
	'maj' => 'Aangepast op ',
	'mentions_legales' => 'Wettelijke vermeldingen',

	// N
	'nom' => 'Naam',
	'nouvelle_suggestion' => 'Nieuwe suggestie',

	// P
	'precedent' => 'Vorige',
	'proposer_suggestion' => 'Stel een verbetering voor!',
	'proposer_suggestion_img' => 'Ticketbeheer',

	// R
	'reponse' => 'Antwoord',

	// S
	'signaler_coquille' => 'Meld een fout…',
	'sinscrire' => 'Registreren',
	'sommaire' => 'Inhoud',
	'sommaire_livre' => 'Inhoud',
	'sous_licence' => 'onder de',
	'suggestion' => 'Suggestie',
	'suggestions' => 'Suggesties',
	'suivant' => 'Volgende',
	'suivi' => 'Volg',
	'suivi_dernieres_modifications_articles' => 'Recent aangepaste artikelen',
	'suivi_derniers_articles' => 'Recente artikelen',
	'suivi_derniers_articles_proposes' => 'Laast voorgestelde artikelen',
	'suivi_derniers_commentaires' => 'Recente opmerkingen',
	'suivi_description' => 'Volg de site...',
	'symboles' => 'Symbolen',

	// T
	'table_des_matieres' => 'Inhoudsopgave',
	'tickets_sur_inscription' => '
		Het maken van tickets en opmerkingen is voorbehouden
		aan geregistreerde gebruikers.
	',
	'titre_articles_lies' => 'Gerelateerde artikelen',
	'titre_identification' => 'Identificatie',
	'titre_inscription' => 'Registratie',
	'tout_voir' => 'Toon alles'
);
